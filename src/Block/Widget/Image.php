<?php

namespace WFN\CMS\Block\Widget;

use Illuminate\Support\Facades\Request;
use Storage;

class Image extends AbstractWidget
{

    const VIEW = 'cms.widget.image';

    public function getImageUrl()
    {
        return Request::root() . Storage::url(\Page::MEDIA_PATH . $this->image);
    }

}