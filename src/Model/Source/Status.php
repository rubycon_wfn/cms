<?php
namespace WFN\CMS\Model\Source;

class Status extends \WFN\Admin\Model\Source\AbstractSource
{

    const ENABLED  = 1;
    const DISABLED = 0;

    protected function _getOptions()
    {
        return [
            self::ENABLED  => 'Enabled',
            self::DISABLED => 'Disabled',
        ];
    }

}