<?php
return [
    'wysiwyg' => [
        'title'  => 'WYSIWYG',
        'class'  => \WFN\CMS\Block\Widget\Wysiwyg::class,
        'fields' => [
            [
                'type'  => 'wysiwyg',
                'name'  => 'content',
                'label' => 'Content',
            ]
        ],
    ],
    'cms_block' => [
        'title'  => 'CMS Block',
        'class'  => \WFN\CMS\Block\Widget\Block::class,
        'fields' => [
            [
                'type'   => 'select',
                'name'   => 'block_id',
                'label'  => 'Choose Block',
                'source' => \WFN\CMS\Model\Source\Blocks::getInstance(),
            ]
        ],
    ],
    'image' => [
        'title'  => 'Image',
        'class'  => \WFN\CMS\Block\Widget\Image::class,
        'fields' => [
            [
                'type'   => 'image',
                'name'   => 'image',
                'label'  => 'Image',
            ]
        ],
    ],
];