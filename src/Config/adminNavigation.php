<?php
return [
    'cms' => [
        'label' => 'CMS',
        'icon'  => 'icon-list',
        'childrens' => [
            'page' => [
                'route' => 'admin.cms.page',
                'label' => 'Pages',
            ],
            'block' => [
                'route' => 'admin.cms.block',
                'label' => 'Blocks',
            ],
        ],
    ],
];