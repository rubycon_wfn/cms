<?php
namespace WFN\CMS\Model\Source;

class Layouts extends \WFN\Admin\Model\Source\AbstractSource
{

    protected $_options = [];

    protected function _getOptions()
    {
        if(!$this->_options) {
            $this->_options['cms.page'] = 'Default';
            foreach(config('pageLayouts', []) as $code => $title) {
                $this->_options[$code] = $title;
            }
        }
        return $this->_options;
    }

}