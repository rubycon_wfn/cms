<?php

namespace WFN\CMS\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class PageController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function view(\Page $page)
    {
        return view($page->layout ?: 'cms.page', compact('page'));
    }

}