<?php
namespace WFN\CMS\Http;

use Illuminate\Routing\Router as AppRouter;
use WFN\CMS\Model\Source\Status;
use Illuminate\Http\Request;

class Router
{

    protected $appRouter;

    public function __construct(AppRouter $router)
    {
        $this->appRouter = $router;
    }

    public function handle($requestUri)
    {
        $requestUri = trim($requestUri, '/');
        
        if(!$requestUri) {
            $homepageIdentifier = \Settings::getConfigValue('cms/homepage_identifier');
            if($page = $this->_getPage($homepageIdentifier)) {
                $request = Request::create('cms/page/view/' . $page->id);
                return $this->appRouter->dispatch($request);
            }
        }

        if($page = $this->_getPageByPath($requestUri)) {
            $request = Request::create('cms/page/view/' . $page->id);
            return $this->appRouter->dispatch($request);
        }

        return abort(404);
    }

    protected function _getPageByPath($requestUri)
    {
        $requestUri = explode('/', $requestUri);
        $requestUri = array_reverse($requestUri);
        $urlKey = array_shift($requestUri);
        $page = $this->_getPage($urlKey);
        if(!$page) {
            return false;
        }
        if(!$this->_checkPageParents($page, $requestUri)) {
            return false;
        }
        return $page;
    }

    protected function _checkPageParents(\Page $page, array $parentUrlKeys)
    {
        if(!$parentUrlKeys) {
            return true;
        }
        $currentPage = $page;
        foreach($parentUrlKeys as $urlKey) {
            if(!$currentPage->parent || $currentPage->parent->url_key != $urlKey) {
                return false;
            }
            $currentPage = $currentPage->parent;
        }
        return true;
    }

    protected function _getPage($urlKey)
    {
        return \Page::where('url_key', $urlKey)->where('status', Status::ENABLED)->first();
    }

}
