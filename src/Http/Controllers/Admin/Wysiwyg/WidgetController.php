<?php

namespace WFN\CMS\Http\Controllers\Admin\Wysiwyg;

use Illuminate\Http\Request;

class WidgetController extends \WFN\Admin\Http\Controllers\Controller
{

    public function detail(Request $request)
    {
        $config = config('widgets', []);
        return !empty($config[$request->input('widget')]) ? $config[$request->input('widget')]['fields'] : [];
    }

}