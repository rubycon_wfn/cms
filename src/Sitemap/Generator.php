<?php

namespace WFN\CMS\Sitemap;

use WFN\CMS\Model\Source\Status;

class Generator extends \WFN\Sitemap\Model\Generator
{

    const TITLE = 'CMS Pages';

    protected $links = [];

    protected $lastMod;

    public function getLastMod()
    {
        $this->_init();
        return $this->lastMod;
    }

    public function getLinks()
    {
        $this->_init();
        return $this->links;
    }

    protected function _init()
    {
        if(!$this->links) {
            foreach(\Page::where('status', Status::ENABLED)->get() as $page) {
                if($page->url_key == \Settings::getConfigValue('cms/homepage_identifier')) {
                    $loc = url('/');
                } else {
                    $loc = $page->getPageUrl();
                }
                if(!$this->lastMod || $this->lastMod->lt($page->updated_at)) {
                    $this->lastMod = $page->updated_at;
                }
                $this->links[] = (object)[
                    'loc'     => $loc,
                    'lastmod' => $page->updated_at
                ];
            }
        }
    }

}