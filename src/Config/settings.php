<?php

return [
    'cms' => [
        'label'  => 'CMS',
        'fields' => [
            'homepage_identifier' => [
                'label' => 'Homepage Identifier',
                'type'  => 'text',
            ],
        ],
    ],
];