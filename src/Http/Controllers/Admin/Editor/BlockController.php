<?php

namespace WFN\CMS\Http\Controllers\Admin\Editor;

use Illuminate\Http\Request;

class BlockController extends \WFN\Admin\Http\Controllers\Controller
{

    public function getBlockHtml($block_id)
    {
        $blocks = config('blocks', []);
        if(empty($blocks[$block_id])) {
            return '';
        }
        $blockData = $blocks[$block_id];
        $blockData['key'] = $block_id;
        return view('admin.widget.form.advanced.block', compact('blockData'));
    }

}