<?php
namespace WFN\CMS\Block\Admin\Page;

class Form extends \WFN\Admin\Block\Widget\AbstractForm
{

    protected $adminRoute = 'admin.cms.page';

    protected function _beforeRender()
    {
        $this->addField('general', 'id', 'ID', 'hidden', ['required' => false]);
        $this->addField('general', 'title', 'Title', 'text', ['required' => true]);
        $this->addField('general', 'status', 'Status', 'select', [
            'required' => false,
            'source'   => \WFN\CMS\Model\Source\Status::class,
        ]);
        $this->addField('general', 'parent_id', 'Parent Page', 'select', [
            'required' => false,
            'source'   => \WFN\CMS\Model\Source\Pages::class,
        ]);
        $this->addField('content', 'content', 'Content', 'editor', ['required' => true]);
        
        $this->addField('design', 'layout', 'Layout', 'select', [
            'required' => true,
            'source'   => \WFN\CMS\Model\Source\Layouts::class,
        ]);
        $this->addField('design', 'image', 'Image', 'image', ['required' => false]);
        $this->addField('design', 'short_description', 'Short Description', 'textarea', ['required' => false]);

        $this->addField('search_engine_optimization', 'url_key', 'URL Key', 'text', ['required' => true]);
        $this->addField('search_engine_optimization', 'meta_title', 'Meta Title', 'text', ['required' => false]);
        $this->addField('search_engine_optimization', 'meta_description', 'Meta Description', 'textarea', ['required' => false]);
        $this->addField('search_engine_optimization', 'og_title', 'OG Title', 'text', ['required' => false]);
        $this->addField('search_engine_optimization', 'og_description', 'OG Description', 'textarea', ['required' => false]);
        $this->addField('search_engine_optimization', 'og_image', 'OG Image', 'image', ['required' => false]);

        return parent::_beforeRender();
    }

}