<?php
namespace WFN\CMS\Model\Source;

class AdvancedBlocks extends \WFN\Admin\Model\Source\AbstractSource
{

    protected $_options = [];

    protected function _getOptions()
    {
        if(!$this->_options) {
            foreach(config('blocks', []) as $code => $data) {
                if(empty($data['class']) || empty($data['title']) || (!isset($data['fields']) && !is_array($data['fields']))) {
                    // Wrong widget declaration
                    continue;
                }
                $this->_options[$code] = $data['title'];
            }
        }
        return $this->_options;
    }

}