<?php
namespace WFN\CMS\Model\Source;

class Pages extends \WFN\Admin\Model\Source\AbstractSource
{

    protected $_options = [];

    protected function _getOptions()
    {
        if(!$this->_options) {
            foreach(\Page::all() as $page) {
                $this->_options[$page->id] = $page->title . ($page->status == Status::DISABLED ? ' (Disabled)' : '');
            }
        }
        return $this->_options;
    }

}