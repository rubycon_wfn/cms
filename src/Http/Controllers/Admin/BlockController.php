<?php

namespace WFN\CMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BlockController extends \WFN\Admin\Http\Controllers\Crud\Controller
{
    
    protected function _init(Request $request)
    {
        $this->gridBlock   = new \WFN\CMS\Block\Admin\Block\Grid($request);
        $this->formBlock   = new \WFN\CMS\Block\Admin\Block\Form();
        $this->entity      = new \WFN\CMS\Model\Block();
        $this->entityTitle = 'CMS - Block';
        $this->adminRoute  = 'admin.cms.block';
        return $this;
    }

    protected function validator(array $data)
    {
        $rules = [
            'title'      => 'required|string|max:255',
            'identifier' => 'required|string|max:255' . ($this->entity->identifier != $data['identifier'] ? '|unique:cms_block' : ''),
            'content'    => 'required',
        ];
        return Validator::make($data, $rules);
    }

}