<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCmsPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_page', function (Blueprint $table) {
            $table->string('meta_title')->after('content')->nullable(true);
            $table->text('meta_description')->after('meta_title')->nullable(true);
            $table->string('og_title')->after('meta_description')->nullable(true);
            $table->text('og_description')->after('og_title')->nullable(true);
            $table->string('og_image')->after('og_description')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
