<?php

namespace WFN\CMS\Model;

use Illuminate\Support\Facades\Request;
use Storage;

class Page extends Block
{

    const MEDIA_PATH = 'page' . DIRECTORY_SEPARATOR;

    protected $table = 'cms_page';

    protected $fillable = ['parent_id', 'status', 'edit_mode', 'url_key', 'title',
        'content', 'layout', 'image', 'short_description', 'meta_title',
        'meta_description', 'og_title', 'og_description', 'og_image'];

    protected $mediaFields = ['image', 'og_image'];

    protected $_entityType = 'page';

    public function parent()
    {
        return $this->hasOne(\Page::class, 'id', 'parent_id');
    }

    public function isHomepage()
    {
        return $this->url_key == \Settings::getConfigValue('cms/homepage_identifier');
    }

    public function getUrlPath()
    {
        if(!$this->path) {
            $path = [$this->url_key];
            $currentPage = $this;
            while($currentPage->parent) {
                $path[] = $currentPage->parent->url_key;
                $currentPage = $currentPage->parent;
            }
            $path = array_reverse($path);
            $this->path = '/' . implode('/', $path);
        }
        return $this->path;
    }

    public function getAttributeUrl($key)
    {
        $value = $this->getAttribute($key);
        if(!$value) {
            return false;
        }
        return Request::root() . Storage::url(static::MEDIA_PATH . $value);
    }

    public function getPageUrl()
    {
        return Request::root() . ($this->isHomepage() ? '' : $this->getUrlPath());
    }

    public function getMetaTitle()
    {
        return $this->meta_title ?: $this->title;
    }

    public function getMetaDescription()
    {
        return $this->meta_description ?: substr(strip_tags($this->getContent()), 0, 250);
    }

    public function getOgTitle()
    {
        return $this->og_title ?: $this->getMetaTitle();
    }

    public function getOgDescription()
    {
        return $this->og_description ?: $this->getMetaDescription();
    }

    public function getOgImageUrl()
    {
        return $this->getAttributeUrl('og_image');
    }

    public function fill(array $attributes)
    {
        foreach($attributes as $key => $value) {
            if(in_array($key, $this->mediaFields) && !empty($value['file']) && $value['file'] instanceof \Illuminate\Http\UploadedFile) {
                $value = $this->_uploadFile($value['file']);
                $attributes[$key] = $value;
            }
        }
        return parent::fill($attributes);
    }

}