<?php

namespace WFN\CMS\Block;

class Breadcrumbs
{

    public static function getCrumbs($page)
    {
        if($page->url_key == \Settings::getConfigValue('cms/homepage_identifier')) {
            return false;
        }

        $crumbs = [];
        $currentPage = $page;
        while($currentPage->parent) {
            $crumbs[] = [
                'url'   => url($currentPage->parent->getUrlPath()),
                'title' => $currentPage->title,
            ];
            $currentPage = $currentPage->parent;
        }

        $crumbs[] = [
            'url'   => url('/'),
            'title' => 'Home',
        ];

        return array_reverse($crumbs);
    }

}