<?php
namespace WFN\CMS\Block\Admin\Block;

class Form extends \WFN\Admin\Block\Widget\AbstractForm
{

    protected $adminRoute = 'admin.cms.block';

    protected function _beforeRender()
    {
        $this->addField('general', 'id', 'ID', 'hidden', ['required' => false]);
        $this->addField('general', 'title', 'Title', 'text', ['required' => true]);
        $this->addField('general', 'identifier', 'Identifier', 'text', ['required' => true]);
        $this->addField('general', 'status', 'Status', 'select', [
            'required' => false,
            'source'   => \WFN\CMS\Model\Source\Status::class,
        ]);
        $this->addField('content', 'content', 'Content', 'editor', ['required' => true]);
        return parent::_beforeRender();
    }

}