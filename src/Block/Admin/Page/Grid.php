<?php
namespace WFN\CMS\Block\Admin\Page;

class Grid extends \WFN\Admin\Block\Widget\AbstractGrid
{

    protected $filterableFields = ['title', 'parent_id', 'url_key', 'status'];

    protected $adminRoute = 'admin.cms.page';

    public function getInstance()
    {
        return new \Page();
    }

    protected function _beforeRender()
    {
        $this->addColumn('id', 'ID', 'text', true);
        $this->addColumn('title', 'Title');
        $this->addColumn('url_key', 'URL Key');
        $this->addColumn('status', 'Status', 'select', true, new \WFN\CMS\Model\Source\Status());
        $this->addColumn('parent_id', 'Parent Page', 'select', true, new \WFN\CMS\Model\Source\Pages());
        return parent::_beforeRender();
    }

    public function getTitle()
    {
        return 'CMS - Pages';
    }

}