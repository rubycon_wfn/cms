<?php

namespace WFN\CMS\Block\Widget;

use WFN\CMS\Model\Source\Status;

class Block extends AbstractWidget
{

    const VIEW = 'cms.widget.block';

    public function render()
    {
        if(empty($this->block_id)) {
            return '';
        }
        $block = \WFN\CMS\Model\Block::where('id', $this->block_id)->orWhere('identifier', $this->block_id)->first();
        return $block && $block->status == Status::ENABLED ? $block->getContent() : '';
    }

}