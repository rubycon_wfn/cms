<?php

namespace WFN\CMS\Block\Widget;

use WFN\CMS\Model\Source\Status;

class Wysiwyg extends AbstractWidget
{

    public function render()
    {
        $filter = new \WFN\CMS\Model\Content\Filter();
        return $filter->filterContent($this->content);
    }

}