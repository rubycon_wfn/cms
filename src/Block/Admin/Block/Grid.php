<?php
namespace WFN\CMS\Block\Admin\Block;

class Grid extends \WFN\Admin\Block\Widget\AbstractGrid
{

    protected $filterableFields = ['title', 'identifier', 'status'];

    protected $adminRoute = 'admin.cms.block';

    public function getInstance()
    {
        return new \WFN\CMS\Model\Block();
    }

    protected function _beforeRender()
    {
        $this->addColumn('id', 'ID', 'text', true);
        $this->addColumn('title', 'Title');
        $this->addColumn('identifier', 'Identifier');
        $this->addColumn('status', 'Status', 'select', true, new \WFN\CMS\Model\Source\Status());
        return parent::_beforeRender();
    }

    public function getTitle()
    {
        return 'CMS - Blocks';
    }

}