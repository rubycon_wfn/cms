<?php

namespace WFN\CMS\Model\Content;

class Filter
{

    public function filterContent($content)
    {
        // Render Static Blocks
        $content = preg_replace_callback('/(<p>)?<span id="[\S\d]+" class="mceNonEditable" contenteditable="false">[\S ]+<\/span>(<\/p>)?/', function($_matches) {
            preg_match_all('/"[\S\d]+"/', $_matches[0], $matches);
            $matches = array_shift($matches);
            $data = base64_decode(trim($matches[0], '"'));
            $data = json_decode($data);
            $widgets = config('widgets', []);
            if(empty($data->widget) || empty($widgets[$data->widget])) {
                return '';
            }
            $widgetClass = $widgets[$data->widget]['class'];
            $widget = new $widgetClass();
            if(!$widget instanceof \WFN\CMS\Block\Widget\AbstractWidget) {
                return '';
            }
            $widget->setData($data);
            return $widget->render();
        }, $content);

        return $content;
    }

}