<?php

$cmsRoutes = [
    'page'  => '\WFN\CMS\Http\Controllers\Admin\PageController',
    'block' => '\WFN\CMS\Http\Controllers\Admin\BlockController',
];

Route::prefix(env('ADMIN_PATH', 'admin'))->group(function() use ($cmsRoutes) {
    Route::prefix('cms')->group(function() use ($cmsRoutes) {
        foreach($cmsRoutes as $route => $controller) {
            Route::prefix($route)->group(function() use ($route, $controller) {
                Route::get('/', $controller . '@index')->name('admin.cms.' . $route);
                Route::get('/edit/{id}', $controller . '@edit')->name('admin.cms.' . $route . '.edit');
                Route::get('/new', $controller . '@new')->name('admin.cms.' . $route . '.new');
                Route::post('/save', $controller . '@save')->name('admin.cms.' . $route . '.save');
                Route::get('/delete/{id}', $controller . '@delete')->name('admin.cms.' . $route . '.delete');
            });
        }
        Route::prefix('wysiwyg')->group(function() {
            Route::prefix('image')->group(function() {
                Route::get('/list', '\WFN\CMS\Http\Controllers\Admin\Wysiwyg\ImageController@list')->name('admin.cms.wysiwyg.image.list');
                Route::post('/upload', '\WFN\CMS\Http\Controllers\Admin\Wysiwyg\ImageController@upload')->name('admin.cms.wysiwyg.image.upload');
            });
            Route::prefix('widget')->group(function() {
                Route::get('/detail', '\WFN\CMS\Http\Controllers\Admin\Wysiwyg\WidgetController@detail')->name('admin.cms.wysiwyg.widget.detail');
            });
        });
        Route::prefix('editor')->group(function() {
            Route::get('/block/{block_id}', '\WFN\CMS\Http\Controllers\Admin\Editor\BlockController@getBlockHtml')->name('admin.cms.editor.block.html');
        });
    });
});

Route::get('/cms/page/view/{page}', 'WFN\CMS\Http\Controllers\PageController@view');

$homepageIdentifier = \Settings::getConfigValue('cms/homepage_identifier');
Route::get($homepageIdentifier, function() {
    return redirect('/');
});

Route::fallback(function($request = '') {
    return App::make(\WFN\CMS\Http\Router::class)->handle($request);
});
