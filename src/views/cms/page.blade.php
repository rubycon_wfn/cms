@extends('layouts.app')

@section('meta_title', $page->getMetaTitle())
@section('meta_description', $page->getMetaDescription())
@section('og_data')
    <meta property="og:title" content="{{ $page->getOgTitle() }}" />
    <meta property="og:description" content="{{ $page->getOgDescription() }}" />
    <meta property="og:type" content="page" />
    <meta property="og:url" content="{{ $page->getPageUrl() }}" />
    @if($page->og_image)
    <meta property="og:image" content="{{ $page->getOgImageUrl() }}" />
    @endif
@endsection

@section('content')
    <div class="container">
        @include('cms.page.breadcrumbs', ['page' => $page])
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{!! $page->title !!}</div>
                    <div class="card-body">{!! $page->getContent() !!}</div>
                </div>
            </div>
        </div>
    </div>
@endsection