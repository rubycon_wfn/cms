<?php

namespace WFN\CMS\Http\Controllers\Admin\Wysiwyg;

use Illuminate\Http\Request;
use Storage;

class ImageController extends \WFN\Admin\Http\Controllers\Controller
{

    const MEDIA_PATH = 'wysiwyg';
    
    public function upload(Request $request)
    {
        $file = $request->file('file');
        if($file && $file instanceof \Illuminate\Http\UploadedFile) {
            $value = $this->_uploadFile($file);
            return ['location' => Storage::url(static::MEDIA_PATH . $value)];
        }
        return false;
    }

    public function list()
    {
        $files = [];
        foreach(Storage::disk('public')->allFiles(static::MEDIA_PATH) as $file) {
            $fileName = explode('/', $file);
            $files[] = [
                'title' => array_pop($fileName),
                'value' => Storage::url($file),
            ];
        }
        return $files;
    }

    protected function _uploadFile($file)
    {
        $path = 'public' . DIRECTORY_SEPARATOR . static::MEDIA_PATH . DIRECTORY_SEPARATOR
            . gmdate('Y') . DIRECTORY_SEPARATOR . gmdate('m') . DIRECTORY_SEPARATOR;
        $fileName = $file->getClientOriginalName();
        if(Storage::disk('local')->exists($path . $fileName)) {
            $iterator = 1;
            while(Storage::disk('local')->exists($path . $fileName)) {
                $fileName = str_replace(
                    '.' . $file->getClientOriginalExtension(),
                    $iterator++ . '.' . $file->getClientOriginalExtension(),
                    $file->getClientOriginalName()
                );
            }
        }

        $file->storeAs($path, $fileName);
        return '/' . gmdate('Y') . '/' . gmdate('m') . '/' . $fileName;
    }


}