<?php
namespace WFN\CMS\Model\Source;

class Widgets extends \WFN\Admin\Model\Source\AbstractSource
{

    protected $_options = [];

    protected function _getOptions()
    {
        if(!$this->_options) {
            $this->_options[''] = 'Please select';
            foreach(config('widgets', []) as $code => $data) {
                if(empty($data['class']) || empty($data['title']) || (!isset($data['fields']) && !is_array($data['fields']))) {
                    // Wrong widget declaration
                    continue;
                }
                $this->_options[$code] = $data['title'];
            }
        }
        return $this->_options;
    }

    public function getWidgetOptionsSelect()
    {
        $options = [];
        foreach($this->getOptions() as $value => $label) {
            $options[] = [
                'value' => (string)$value,
                'text'  => $label,
            ];
        }
        return $options;
    }

}