<?php
return [
    'cms_block' => [
        'title'  => 'CMS Block',
        'class'  => \WFN\CMS\Block\Widget\Block::class,
        'fields' => [
            [
                'type'  => 'selectbox',
                'name'  => 'block_id',
                'label' => 'Choose Block',
                'items' => \WFN\CMS\Model\Source\Blocks::getInstance()->getWidgetOptionsSelect(),
            ]
        ],
    ],
];