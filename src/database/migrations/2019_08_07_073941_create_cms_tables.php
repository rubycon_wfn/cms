<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_page', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('parent_id')->nullable(true);
            $table->tinyInteger('status');
            $table->string('url_key');
            $table->string('title');
            $table->text('content');

            $table->timestamps();

            $table->foreign('parent_id')
                ->references('id')
                ->on('cms_page')
                ->onDelete('set null');
        });

        Schema::create('cms_block', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->tinyInteger('status');
            $table->string('identifier')->unique();
            $table->string('title');
            $table->text('content');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_tables');
    }
}
