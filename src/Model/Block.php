<?php

namespace WFN\CMS\Model;

use Illuminate\Database\Eloquent\Model;
use Storage;
use WFN\CMS\Model\Source\Status;

class Block extends Model
{

    const MEDIA_PATH = 'cms' . DIRECTORY_SEPARATOR;
    
    protected $table = 'cms_block';

    protected $fillable = ['status', 'edit_mode', 'identifier', 'title', 'content'];

    protected $_entityType = 'block';

    public function getType()
    {
        return $this->_entityType;
    }

    public function getContent()
    {
        if($this->status == Status::DISABLED) {
            return '';
        }

        if(!$this->_filteredContent) {
            if($this->edit_mode == \WFN\CMS\Model\Source\EditMode::DEFAULT) {
                $filter = new \WFN\CMS\Model\Content\Filter();
                $this->_filteredContent = $filter->filterContent($this->content);
            } else {
                $blocks = config('blocks', []);
                $this->_filteredContent = '';
                foreach($this->getAdvancedContent() as $key => $data) {
                    $keys = array_keys($data);
                    $blockId = array_shift($keys);
                    if(empty($blocks[$blockId])) {
                        continue;
                    }
                    $blockData = $blocks[$blockId];
                    $block = new $blockData['class']();
                    $block->setData($data[$blockId]);
                    $this->_filteredContent .= $block->render();
                }
                $this->_filteredContent;
            }
        }
        return $this->_filteredContent;
    }

    public function fill(array $attributes)
    {
        $editMode = !empty($attributes['edit_mode']) ? $attributes['edit_mode'] : $this->edit_mode;
        foreach($attributes as $key => $value) {
            if($key == 'content' && $editMode == \WFN\CMS\Model\Source\EditMode::ADVANCED) {
                $attributes[$key] = $this->_prepareAdvancedContent($value);
            }
        }
        return parent::fill($attributes);
    }

    public function getAdvancedContent()
    {
        return json_decode($this->content, true) ?: [];
    }

    protected function _prepareAdvancedContent($content)
    {
        $prepared = [];
        foreach($content as $key => $data) {
            foreach($data as $blockId => $values) {
                $blockData = [];
                foreach($values as $field => $value) {
                    if(!empty($value['file']) && $value['file'] instanceof \Illuminate\Http\UploadedFile) {
                        $value = $this->_uploadFile($value['file']);
                        $value = Storage::url(static::MEDIA_PATH . $value);
                    } elseif(is_array($value)) { // Rows Content OR Multiselect
                        $preparedValue = [];
                        foreach($value as $rowNumber => $rowData) {
                            if(!is_array($rowData)) {
                                $preparedRowValue = $rowData;
                            } else {
                                $preparedRowValue = [];
                                foreach($rowData as $_key => $_value) {
                                    if(!empty($_value['file']) && $_value['file'] instanceof \Illuminate\Http\UploadedFile) {
                                        $_value = $this->_uploadFile($_value['file']);
                                        $_value = Storage::url(static::MEDIA_PATH . $_value);
                                    }
                                    $preparedRowValue[$_key] = $_value;
                                }
                            }
                            $preparedValue[$rowNumber] = $preparedRowValue;
                        }
                        $value = $preparedValue;
                    }
                    $blockData[$field] = $value;
                }
                $prepared[$key] = [
                    $blockId => $blockData
                ];
            }
        }
        return json_encode($prepared);
    }

    protected function _uploadFile($file)
    {
        $path = 'public' . DIRECTORY_SEPARATOR . static::MEDIA_PATH;
        $fileName = $file->getClientOriginalName();
        $path .= substr($fileName, 0, 1) . DIRECTORY_SEPARATOR . substr($fileName, 1, 1) . DIRECTORY_SEPARATOR;
        if(Storage::disk('local')->exists($path . $fileName)) {
            $iterator = 1;
            while(Storage::disk('local')->exists($path . $fileName)) {
                $fileName = str_replace(
                    '.' . $file->getClientOriginalExtension(),
                    $iterator++ . '.' . $file->getClientOriginalExtension(),
                    $file->getClientOriginalName()
                );
            }
        }

        $file->storeAs($path, $fileName);
        return substr($fileName, 0, 1) . DIRECTORY_SEPARATOR . substr($fileName, 1, 1) . DIRECTORY_SEPARATOR . $fileName;
    }
}