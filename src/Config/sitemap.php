<?php

return [
    'generators' => [
        'pages' => \WFN\CMS\Sitemap\Generator::class,
    ],
];