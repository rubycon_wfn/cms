<?php


namespace WFN\CMS\Model\Source;

use  WFN\Blog\Model\Author;

class Authors extends \WFN\Admin\Model\Source\AbstractSource
{
    protected $_options = [];

    protected function _getOptions()
    {
        if(!$this->_options) {
            foreach(Author::all() as $author) {
                $this->_options[$author->id] = $author->name;
            }
        }
        return $this->_options;
    }
}
