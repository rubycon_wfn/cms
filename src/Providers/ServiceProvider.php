<?php
namespace WFN\CMS\Providers;

use WFN\Admin\Providers\ServiceProvider as WFNServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;

class ServiceProvider extends WFNServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->booting(function() {
            $loader = AliasLoader::getInstance();
            $loader->alias('Page', \WFN\CMS\Model\Page::class);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $basePath = realpath(__DIR__ . '/..');
        
        $this->loadViewsFrom($basePath . '/views', 'cms');
        $this->publishes([
            $basePath . '/views' => resource_path('views'),
        ], 'view');

        $this->publishes([
            $basePath . '/assets' => public_path('adminhtml'),
        ], 'public');

        $this->loadMigrationsFrom($basePath . '/database/migrations');
        $this->mergeConfigFrom($basePath . '/Config/sitemap.php', 'sitemap');
        $this->mergeConfigFrom($basePath . '/Config/widgets.php', 'widgets');
        $this->mergeConfigFrom($basePath . '/Config/blocks.php', 'blocks');
        $this->mergeConfigFrom($basePath . '/Config/pageLayouts.php', 'pageLayouts');
        $this->mergeConfigFrom($basePath . '/Config/adminNavigation.php', 'adminNavigation');
        $this->mergeConfigFrom($basePath . '/Config/settings.php', 'settings');

        Route::middleware('web')->group($basePath . '/routes/web.php');

        $router->aliasMiddleware('cms', \WFN\CMS\Http\Middleware\Router::class);
    }

}
