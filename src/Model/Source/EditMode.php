<?php
namespace WFN\CMS\Model\Source;

class EditMode extends \WFN\Admin\Model\Source\AbstractSource
{

    const DEFAULT  = 1;
    const ADVANCED = 2;

    protected function _getOptions()
    {
        return [
            self::DEFAULT  => 'Default',
            self::ADVANCED => 'Advanced',
        ];
    }

}