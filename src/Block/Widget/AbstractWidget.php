<?php

namespace WFN\CMS\Block\Widget;

use View;

class AbstractWidget
{

    const VIEW = '';

    public function setData($data)
    {
        foreach($data as $key => $value) {
            $this->{$key} = $value;
        }
        return $this;
    }

    public function render()
    {
        if(!View::exists(static::VIEW)) {
            return '[Widget "' . static::class . '" can not render]';
        }
        $widget = $this;
        return view(static::VIEW, compact('widget'));
    }

    public static function getInstance()
    {
        return new static();
    }

}