<?php
namespace WFN\CMS\Model\Source;

class Blocks extends Widgets
{

    protected function _getOptions()
    {
        if(!$this->_options) {
            try {
                foreach(\WFN\CMS\Model\Block::all() as $block) {
                    $this->_options[$block->id] = $block->title . ($block->status == Status::DISABLED ? ' (Disabled)' : '');
                }
            } catch (\Exception $e) {
                // pass
            }
        }
        return $this->_options;
    }

}