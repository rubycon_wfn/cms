<?php

namespace WFN\CMS\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends \WFN\Admin\Http\Controllers\Crud\Controller
{

    protected function _init(Request $request)
    {
        $this->gridBlock   = new \WFN\CMS\Block\Admin\Page\Grid($request);
        $this->formBlock   = new \WFN\CMS\Block\Admin\Page\Form();
        $this->entity      = new \Page();
        $this->entityTitle = 'CMS - Page';
        $this->adminRoute  = 'admin.cms.page';
        return $this;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title'     => 'required|string|max:255',
            'url_key'   => 'required|string|max:255',
            'content'   => 'required',
            'parent_id' => 'nullable|exists:cms_page,id'
        ]);
    }

}